<?php //Edit A thon Stat Index page  
$config = require_once 'config.php';
?>
<!doctype html>
<html ng-app="wikiApp">
  <head>
	    <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wikipedia Edit a thon Staticstics App</title>
    <meta name="Description"  content="Wikipedia Edit A Thon statistics app using angular js and quarry">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
     <style>
    body {padding-top: 50px;}
    .page {
      width: 1200px;
      text-align: center;
      margin: 0 auto;
    }
		.grid {
			  width: auto;
			  height: 500px;
			}
			.graph1 {
        width: auto;
        height: auto;
      }
			a {text-decoration:none;} h1{padding-top:50px;}
      #mainBox {height: 250px;}
      #mainHd {background: url('img/pens.jpg') right bottom fixed; background-size: cover; }
      #mainHd h1 {color: #fff; font-weight: bold; text-shadow: 0px 0px 4px #333;}
      #mainHd p {color: #fff;
            font-size: 1.4em;
            font-weight: bold;
            text-shadow: 0px 0px 4px #333;
      }
       #mainHd a {color: #dfff00;}
      .big {font-weight: bold; color: #2780E3; font-size: 4em; text-align: center; border: 1px solid #2780E3;}
      .centerp {text-align: center; color: #333; font-size: 1.4em;}
      .footer {
          text-align: center;
          padding: 30px 0;
          margin-top: 70px;
          border-top: 1px solid #0048de;
          background-color: #dedede;
      }
      .onHead {
      	font-weight: bold; font-size: 3.4em; color: #2780E3;padding: 10px 5px; border-bottom: 1px solid #aaa; margin-bottom: 20px;
      }
      .itmList {
      	border: 1px solid #2780E3; margin:10px 2px; padding: 25px; font-size: 1.8em; margin:20px 5px; color: #333;
      }
      .itmList a {font-weight:bold;}
	.run {color:#f99;font-weight:bold;}
	</style>
  </head>
  <body ng-app="wikiApp">
  <div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" ng-click="isCollapsed = !isCollapsed">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand visible-xs" href="#">Wiki Stats</a>
      </div>
      <nav class="hidden-xs">
        <ul class="nav navbar-nav">
          <li>
            <a href="#top" role="button" class="navbar-brand">
              Wiki Stat
            </a>
          </li>
          <li><a href="#getting_started">Malayalam</a></li>
          <li><a href="#userwise">2024</a></li>
          <li><a href="#userwise">2018</a></li>
          <li><a href="#userwise">2017</a></li>
          <li><a href="#datewise">2016</a></li>
          <li><a href="#userwise">2015</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <div class="container-fluid" id="mainHd">
  <div class="text-center aspect-ratio" id="mainBox">
    <div class="header">
        <h1>
         Statistics of  Edit a thons in Wikipedia
        </h1>
        <p>This tool gives and indepth staticstics of Edit a thons happened in Wikipedia</p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12 col-sm-12 text-center ">
            <h2 class="onHead">Wikipedia Edit-a-thons</h2>
            <?php foreach ($config as $key => $value) { ?>
            <p class="itmList">
              <a href="campaign.php?capval=<?=$key?>"><?=$value['cname']?></a>
               <br/><?=$value['duration']?>
               <br/><a href="<?=$value['url']?>" target="_blank"><?=$value['wiki']?></a>
              </p>
            <?php } ?>
     </div>
  </div>
</div>
<span id="credits">.</span>
<footer class="footer">
    <div class="container">
      <p>Designed and built by <a href="https://github.com/ranjithsiji" target="_blank">Ranjithsiji</a></p>
      <p>Code licensed under <a href="https://github.com/jtblin/angular-chart.js/blob/master/LICENSE">BSD License</a>.</p>
      <p>Source Code on <a href="https://gitlab.wikimedia.org/toolforge-repos/editathonstat">Wikimedia Gitlab</a></p>
      <p><strong>Credits</strong>: <a href="https://github.com/jtblin/angular-chart.js">Angular Chart</a>, <a href="http://www.chartjs.org/">Chart.js</a>, <a href="https://angularjs.org/">AngularJS</a> and  <a href="http://getbootstrap.com">Bootstrap</a></p>
      <p><strong>Data</strong>: From Malayalam Wikipedia Labs DB <a href="https://tools.wmflabs.org/">Quarry</a></p>
    </div>
  </footer>
</body>
</html>
