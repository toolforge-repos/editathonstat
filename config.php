<?php
/* The configuration file for Editathon Stats tool
 * The Short Code
 * Campaign Name
 * Duration
 * Wiki
 * This configuration is a array with the above values
 */

return [
    'ieg24' => [
            'cname' => 'India General Election Edit-a-thon 2024',
            'year' => 2024,
            'duration' => '15th April 2024 to 15th June 2024',
            'desc' => 'Articles about Asia Education. Minimum 300 words and 3000 bytes.',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => 'https://ml.wikipedia.org/wiki/WP:IGE2024',
            'cat'   => "2024ലെ_ഇന്ത്യൻ_പൊതു_തിരഞ്ഞെടുപ്പ്_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ",
    ],
    'faf2024' => [
        'cname' => 'Feminism and Folklore Edit a Thon 2024',
        'year' => 2020,
        'duration' => '1st February 2024 - 31th March 2024',
        'desc' => 'Feminism and Folklore Edit a Thon',
        'wiki' => 'Malayalam Wikipedia',
        'url'  => "https://ml.wikipedia.org/wiki/WP:FAF2024",
        'cat'   => "ഫെമിനിസം_ആന്റ്_ഫോക്ലോർ_(2024)_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam2023' => [
            'cname' => 'Wiki Asian Month 2023',
            'year' => 2023,
            'duration' => '1st November 2023 - 30th November 2023',
            'desc' => 'Part of Wiki Asian Month Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  =>   "https://ml.wikipedia.org/wiki/WP:WAM2023",
            'cat'   => "2023_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'iim23' => [
            'cname' => 'Indian Independence Movement Edit a Thon 2023',
            'year' => 2023,
            'duration' => '15th July 2023 - 15th August 2023 ',
            'desc' => 'Edit a thon based on Indian Independence',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:IIM2023",
            'cat'   => "സ്വാതന്ത്ര്യോത്സവ_(2023)_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'faf2023' => [
        'cname' => 'Feminism and Folklore Edit a Thon 2023',
        'year' => 2020,
        'duration' => '1st February 2023 - 31th March 2023',
        'desc' => 'Feminism and Folklore Edit a Thon',
        'wiki' => 'Malayalam Wikipedia',
        'url'  => "https://ml.wikipedia.org/wiki/WP:FAF2023",
        'cat'   => "ഫെമിനിസം_ആന്റ്_ഫോക്ലോർ_(2023)_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wh2023' => [
            'cname' => 'Women\'s Health Edit-a-thon 2023',
            'year' => 2023,
            'duration' => '1st January 2023 to 31st January 2023',
            'desc' => 'Articles about Asia Education. Minimum 300 words and 3000 bytes.',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => 'https://ml.wikipedia.org/wiki/WP:WH2023',
            'cat'   => "2023_-ലെ_'സ്ത്രീകളുടെ_ആരോഗ്യം'_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ",
    ],
    'eg2022' => [
            'cname' => 'Ente Gramam Edit a Thon 2022',
            'year' => 2022,
            'duration' => '6 March, 2022 - 31 March, 2022',
            'desc' => 'Part of Ente Gramam Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:EG2022",
            'cat'   => "2022_-ലെ_എന്റെ_ഗ്രാമം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam22' => [
            'cname' => 'Wiki Asian Month 2022',
            'year' => 2022,
            'duration' => '1st November 2022 - 30th November 2022',
            'desc' => 'Part of Wiki Asian Month Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  =>   "https://ml.wikipedia.org/wiki/WP:WAM2022",
            'cat'   => "2022_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'faf2022' => [
            'cname' => 'Feminism and Folklore Edit a Thon 2022',
            'year' => 2020,
            'duration' => '1st February 2022 - 31th March 2022',
            'desc' => 'Feminism and Folklore Edit a Thon',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:FAF2022",
            'cat'   => "ഫെമിനിസം_ആന്റ്_ഫോക്ലോർ_(2022)_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam21' => [
            'cname' => 'Wiki Asian Month 2021',
            'year' => 2021,
            'duration' => '1st November 2021 - 30th November 2021',
            'desc' => 'Part of Wiki Asian Month Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  =>   "https://ml.wikipedia.org/wiki/WP:WAM2021",
            'cat'   => "2021_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'faf2021' => [
            'cname' => 'Feminism and Folklore Edit a Thon 2021',
            'year' => 2020,
            'duration' => '1st February 2021 - 31th March 2021',
            'desc' => 'Feminism and Folklore Edit a Thon',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:FAF2021",
            'cat'   => "ഫെമിനിസം_ആന്റ്_ഫോക്ലോർ_(2021)_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    've2021' => [
            'cname' => 'Vaccine Edit-a-thon 2021',
            'year' => 2021,
            'duration' => '8th May 2021 to 31th May 2021',
            'desc' => 'Articles about Vaccines. Minimum 300 words and 3000 bytes.',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => 'https://ml.wikipedia.org/wiki/WP:VE2021',
            'cat'   => "വാക്സിൻ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ",
    ],
    'wam20' => [
            'cname' => 'Wiki Asian Month 2020',
            'year' => 2018,
            'duration' => '1st November 2020 - 30th November 2020',
            'desc' => 'Part of Wiki Asian Month Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  =>   "https://ml.wikipedia.org/wiki/WP:WAM2020",
            'cat'   => "2020_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wlw20ml' => [
            'cname' => 'Wiki Loves Women Edit a Thon 2020',
            'year' => 2020,
            'duration' => '1st February 2020 - 31th March 2020',
            'desc' => 'Wiki Loves Women Edit a Thon',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:WLW20",
            'cat'   => "വിക്കി_ലൗസ്_വിമെൻ_(2020)_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'eg2019' => [
            'cname' => 'Ente Gramam Edit a Thon 2019',
            'year' => 2019,
            'duration' => '9 Dec, 2019 - 31 Dec, 2019',
            'desc' => 'Part of Ente Gramam Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:EG2018",
            'cat'   => "2019_-ലെ_എന്റെ_ഗ്രാമം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam19' => [
            'cname' => 'Wiki Asian Month 2019',
            'year' => 2018,
            'duration' => '1st November 2019 - 30th November 2019',
            'desc' => 'Part of Wiki Asian Month Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  =>   "https://ml.wikipedia.org/wiki/WP:WAM2019",
            'cat'   => "2019_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'pt19' => [
            'cname' => 'Project Tiget Edit-a-thon 2019',
            'year' => 2019,
            'duration' => '10th October 2019 to 10th January 2020',
            'desc' => 'Project Tiger contest. Minimum 300 words and 3000 bytes.',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => 'https://ml.wikipedia.org/wiki/WP:TIGER_2.0',
            'cat'   => "2019ലെ_പ്രോജക്റ്റ്_ടൈഗർ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ",
    ],
    'wlw19ml' => [
            'cname' => 'Wiki Loves Women Edit a Thon 2019',
            'year' => 2019,
            'duration' => '10th February 2019 - 31th March 2019',
            'desc' => 'Wiki Loves Women Edit a Thon',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:WLW19",
            'cat'   => "2019ലെ_വിക്കി_ലൗസ്_വിമെൻ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wlw19as' => [
            'cname' => 'Wiki Loves Women 2019 Edit a Thon',
            'year' => 2019,
            'duration' => '10th February 2019 to 31st March 2019',
            'desc' => 'Wiki Loves Women Edit a Thon',
            'wiki' => 'Asamese Wikipedia',
            'url'  => 'https://as.wikipedia.org/wiki/ৱিকিপিডিয়া:ৱিকি_লাভ্‌ছ_ৱুমেন_২০১৯',
            'cat'   => "ৱিকি_লাভ্‌ছ_ৱুমেন_ভাৰত_২০১৯"
    ],
    'eg2018' => [
            'cname' => 'Ente Gramam Edit a Thon 2018',
            'year' => 2018,
            'duration' => '13th December 2018 - 20th January 2018',
            'desc' => 'Part of Ente Gramam Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:EG2018",
            'cat'   => "2018_-ലെ_എന്റെ_ഗ്രാമം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam18' => [
            'cname' => 'Wiki Asian Month 2018',
            'year' => 2018,
            'duration' => '1st November 2018 - 30th November 2018',
            'desc' => 'Part of Wiki Asian Month Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  =>   "https://ml.wikipedia.org/wiki/WP:WAM2018",
            'cat'   => "2018_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'iim18' => [
            'cname' => 'Indian Independence Movement Edit a Thon 2018',
            'year' => 2018,
            'duration' => '15th August 2018 - 2nd October 2018 ',
            'desc' => 'Edit a thon based on Indian Independence',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:IIM2018",
            'cat'   => "2018ലെ_ഇന്ത്യൻ_സ്വാതന്ത്ര്യ_സമര_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'pt18ml' => [
            'cname' => 'Project Tiger Competetion 2018',
            'year' => 2018,
            'duration' => 'March 2018 - May 2018',
            'desc' => 'Edit a thon part of Project Tiger by CIS-A2K',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:TIGER",
            'cat'   => "2018ലെ_പ്രോജക്റ്റ്_ടൈഗർ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'whmin2018' => [
            'cname' => 'International Woman\'s Day 2018',
            'year' => 2018,
            'duration' => 'March 2018',
            'desc' => 'Edit a thon part of International Women\'s Day',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:WHMIN18",
            'cat'   => "2018-ലെ_വനിതാദിന_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'imid18bn' => [
            'cname' => 'International Mother Langauge Day',
            'year' => 2018,
            'duration' => 'February 2018',
            'desc' => 'Edit a thon part of International Mother Langauge Day',
            'wiki' => 'Bangladesh Wikipedia',
            'url'  => "https://bn.wikipedia.org/s/ape6",
            'cat'   => "নিবন্ধ_প্রতিযোগিতা_২০১৮-এ_তৈরিকৃত_নিবন্ধ"
    ],
    'tw17' => [
            'cname' => 'Thousand Wiki Lights',
            'year' => 2017,
            'duration' => 'December 2017 -- January 2018',
            'desc' => 'Edit a thon part of Thousand Wiki Lights project',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:TWL",
            'cat'   => "ആയിരം_വിക്കി_ദീപങ്ങൾ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam17' => [
            'cname' => 'Wikipedia Asian Month 2017',
            'year' => 2017,
            'duration' => 'November 2017',
            'desc' => 'Edit a thon part of Wikipedia Asian Month',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:WAM2017",
            'cat'   => "2017_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'edu17' => [
            'cname' => 'Education Edit a thon 2017',
            'year' => 2017,
            'duration' => ' August - October 2017',
            'desc' => 'Education Edit a thon',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:EDU17",
            'cat'   => "2017-ലെ_വിദ്യാഭ്യാസ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'nmd17' => [
            'cname' => 'Nelson Mandela Day Edit a thon 2017',
            'year' => 2017,
            'duration' => 'July 2017',
            'desc' => 'Edit a thon conducted as a part of Nelson Mandela Day',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:NMD17",
            'cat'   => "2017-ലെ_നെൽസൺ_മണ്ടേല_ദിന_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wed17' => [
            'cname' => 'World Environment Day Edit a thon 2017',
            'year' => 2017,
            'duration' => 'June 2017',
            'desc' => 'Edit a thon conducted as a part of World Environment Day',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "https://ml.wikipedia.org/wiki/WP:WED17",
            'cat'   => "2017-ലെ_ലോക_പരിസ്ഥിതിദിന_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'unesco17' => [
            'cname' => 'World Heritage Edit a thon 2017',
            'year' => 2017,
            'duration' => 'May 2017',
            'desc' => 'Edit a thon conducted as a part of World Heritage Campaign',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "http://ml.wikipedia.org/wiki/WP:UNESCO2017",
            'cat'   => "2017-ലെ_ലോക_പൈതൃക_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ" 
    ],
    'books17' => [
            'cname' => 'International Books Day Edit a thon 2017',
            'year' => 2017,
            'duration' => 'May 2017',
            'desc' => 'Edit a thon conducted as a part of International Books Day',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "http://ml.wikipedia.org/wiki/WP:BOOK17",
            'cat'   => "2017-ലെ_പുസ്തകദിന_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'whmin2017' => [
            'cname' => 'International Woman\'s Day Edit a thon 2017',
            'year' => 2017,
            'duration' => 'March 2017',
            'desc' => 'Edit a thon conducted as a part of International Woman\'s Day',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "http://ml.wikipedia.org/wiki/WP:WHMIN17",
            'cat'   => "2017-ലെ_വനിതാദിന_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'odia17' => [
            'cname' => 'International Woman\'s Day Edit a thon 2017',
            'year' => 2017,
            'duration' => 'March 2017',
            'desc' => 'Edit a thon conducted as a part of International Woman\'s Day',
            'wiki' => 'Odia Wikipedia',
            'url'  => "https://or.wikipedia.org/s/13k6",
            'cat'   => "୨୦୧୭-ମହିଳା_ଇତିହାସ_ମାସରେ_ଗଢ଼ା_ବା_ଉନ୍ନତ_କରାଯାଇଥିବା_ପ୍ରସଙ୍ଗମାନ"
    ],
    'olympics16' => [
            'cname' => 'Rio Olympics Edit a thon 2016',
            'year' => 2016,
            'duration' => '29th July 2016 to 18th September 2016',
            'desc' => 'Edit a thon conducted as a part of Rio Olympics 2016',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "hhttp://ml.wikipedia.org/wiki/WP:RIO2016",
            'cat'   => "2016-ലെ_റിയോ_ഒളിമ്പിക്സിലെ_ഇന്ത്യ_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam16' => [
            'cname' => 'Wikipedia Asian Month 2016',
            'year' => 2016,
            'duration' => 'November 2016',
            'desc' => 'Edit a thon conducted as a part of Wikipedia Asian Month',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "http://ml.wikipedia.org/wiki/WP:WAM16",
            'cat'   => "2016_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],
    'wam15' => [
            'cname' => 'Wikipedia Asian Month 2015',
            'year' => 2015,
            'duration' => 'November 2016',
            'desc' => 'Edit a thon conducted as a part of Wikipedia Asian Month',
            'wiki' => 'Malayalam Wikipedia',
            'url'  => "http://ml.wikipedia.org/wiki/WP:WAM15",
            'cat'   => "2015_-ലെ_വിക്കിപീഡിയ_ഏഷ്യൻ_മാസം_തിരുത്തൽ_യജ്ഞത്തിന്റെ_ഭാഗമായി_സൃഷ്ടിക്കപ്പെട്ട_ലേഖനങ്ങൾ"
    ],


];